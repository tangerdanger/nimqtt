# Package
import strformat

version       = "0.1.0"
author        = "Ryan Cotter"
description   = "An MQTT broker"
license       = "MIT"
srcDir        = "src"
skipDirs      = @["private"]

let 
  BIN = "bin"
  TESTS = "tests"

# Dependencies

requires "nim >= 0.20.0"
requires "websocket#head"

task run, "Run the MQTT 5 tests":
  exec fmt"mkdir -p {BIN}"
  exec fmt"nim c -r -o:{BIN}/mqtt5 src/nimqtt.nim"

task runServer, "Run the MQTT Server":
  exec fmt"mkdir -p {BIN}"
  exec fmt"nim c -r -o:{BIN}/server src/nimqtt/server.nim"

task runWithClient, "Run MQTT 5 tests with websocket client enabled":
  exec fmt"mkdir -p {BIN}"
  exec fmt"nim c -r -d:initClient -o:{BIN}/mqtt5 src/nimqtt.nim"

task runSocketTest, "Run a socket test":
  exec fmt"mkdir -p {BIN}"
  exec fmt"nim c -r -o:{BIN}/stest tests/socketTest.nim"

task runForwarderTests, "Run tests on the Forwarder":
  exec fmt"mkdir -p {BIN}"
  exec fmt"nim c -r -o:{BIN}/forwarder src/nimqtt/forwarder.nim"

task runSSLForwarder, "Run the SSL Forwarder":
  exec fmt"mkdir -p {BIN}"
  exec fmt"nim c -r -d:ssl -o:{BIN}/forwarder src/nimqtt/forwarder.nim"

