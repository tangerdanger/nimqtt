import endians, nativesockets, unicode, tables, strformat, asyncdispatch, websocket, sequtils
import nimqtt/private/[types, helpers, dollars]
import nimqtt/client

proc initSubscriptionTopicFilter*(topic: UTF8String, reserved, retainHandling, rap, nl, qos: uint8): MQTTSubscriptionTopicFilter =
  result = MQTTSubscriptionTopicFilter()
  result.topicFilter = topic

  result.subscriptionOptions = MQTTSubscribeOptions(
    reserved: reserved,
    retainHandling: retainHandling, 
    rap: rap,
    nl: nl,
    qos: qos
  )

proc initSubscriptionTopicFilter*(filter: TopicFilterData): MQTTSubscriptionTopicFilter =
  initSubscriptionTopicFilter(filter.topic, filter.reserved, filter.retainHandling, filter.rap, filter.nl, filter.qos)

proc initSubscriptionTopicFilters*(filters: seq[TopicFilterData]): seq[MQTTSubscriptionTopicFilter] =
  map(filters, initSubscriptionTopicFilter)

proc initTopicFilter*(topic: UTF8String): MQTTTopicFilter =
  MQTTTopicFilter(topic: topic)

proc initTopicFilters*(topics: seq[UTF8String]): seq[MQTTTopicFilter] =
  map(topics, initTopicFilter)

proc decodeVarBytes(bytes: MQTTVarByteStream): uint =
  ## Decompilation proc for VarBytes
  assert bytes.len <= 4
  result = 0
  var
    multiplier = 1
    encodedByte: MQTTVarByte
    index = 0

  doWhile (encodedByte and 128.uint) != 0:
    encodedByte = bytes[index] #Get next byte in stream
    index.inc
    result += (encodedByte and 127.uint) * multiplier.uint
    if multiplier > 128*128*128:
      raise newException(DecodeError, fmt"Error decoding var byte: {$bytes}")
    multiplier *= 128

proc encodeVarBytes*(value: SomeUnsignedInt): MQTTVarByteStream =
  ## Given an unsigned int, compile it into a sequence of VarBytes
  result = @[]
  var
    varValue = value
    encodedByte: MQTTVarByte

  doWhile varValue.int > 0:
    encodedByte = (varValue mod 128).toMQTTVarByte
    varValue = (varValue div 128)
    if varValue.int > 0:
      encodedByte = (encodedByte or 128.uint).toMQTTVarByte
    result.add(encodedByte)

  assert result.len <= 4

proc encodeVarBytes*(value: SomeSignedInt): MQTTVarByteStream =
  encodeVarBytes(value.uint)

proc initMQTTProperty*(kind: MQTTPropertyCode): MQTTProperty =
  result = MQTTProperty(kind: kind)
  result.id = encodeVarBytes(ord(kind).uint)

proc initMQTTConnectPacket*(): MQTTConnectPacket =
  result = MQTTConnectPacket()
  
  #Control type has a type opcode of 0x1 
  result.fixedHeader = MQTTFixedHeader(
    header: MQTTFixedHeaderFields(headerType: ctConnect, flags: 0),
    remaining: @[]
  )

  let cFlags = MQTTConnectFlags(
    reserved: 0,
    cleanStart: 1,
    willFlag: 0,
    willQos: 0,
    willRetain: 0,
    password: 0,
    username: 0
  )

  let properties: seq[MQTTProperty] = @[
    initMQTTProperty(kind = mpcSessionExpiry),
    initMQTTProperty(kind = mpcReceiveMax),
    initMQTTProperty(kind = mpcMaxPackSize),
    initMQTTProperty(kind = mpcTopicAliasMax),
    initMQTTProperty(kind = mpcReqRespInfo),
    initMQTTProperty(kind = mpcReqInfo),
    initMQTTProperty(kind = mpcUserProperty),
    initMQTTProperty(kind = mpcAuthMethod),
    initMQTTProperty(kind = mpcAuthData)
  ]

  result.variableHeader = MQTTConnectVarHeader(
    protocolName: procotolName,
    connectFlags: cFlags,
    keepAlive: MQTTKeepAlive(kaMSB: 0, kaLSB: KeepAlive),
    properties: MQTTProperties(length: encodeVarBytes(properties.sizeof), data: properties)
  )

  let willProperties = @[
    initMQTTProperty(kind = mpcWillDelayInterval),
    initMQTTProperty(kind = mpcPayloadFormatIndicator),
    initMQTTProperty(kind = mpcMessageExpiryInterval),
    initMQTTProperty(kind = mpcContentType),
    initMQTTProperty(kind = mpcResponseTopic),
    initMQTTProperty(kind = mpcCorrelationData),
    initMQTTProperty(kind = mpcUserProperty),
  ]

  result.payload = MQTTConnectPayload(
    clientId: @[],
    will: MQTTProperties(
      length: encodeVarBytes(willProperties.sizeof),
      data: willProperties
    ),
    willTopic: @[],
    willPayload: BinData(length: 0, data: @[]),
    username: @[],
    password: BinData(length: 0, data: @[])
  )

proc initMQTTConnackPacket*(): MQTTConnackPacket =
  result = MQTTConnackPacket()
  
  #Control type has a type opcode of 0x1 
  result.fixedHeader = MQTTFixedHeader(
    header: MQTTFixedHeaderFields(headerType: ctConnack, flags: 0),
    remaining: @[]
  )

  let cFlags = MQTTConnackFlags(
    reserved: 0,
    sessionPresent: 0
  )

  let properties: seq[MQTTProperty] = @[
    initMQTTProperty(kind = mpcSessionExpiry),
    initMQTTProperty(kind = mpcReceiveMax),
    initMQTTProperty(kind = mpcMaxQOS),
    initMQTTProperty(kind = mpcRetainAvailable),
    initMQTTProperty(kind = mpcMaxPackSize),
    initMQTTProperty(kind = mpcAssignedClientId),
    initMQTTProperty(kind = mpcTopicAliasMax),
    initMQTTProperty(kind = mpcReason),
    initMQTTProperty(kind = mpcUserProperty),
    initMQTTProperty(kind = mpcWildcardSubAvailable),
    initMQTTProperty(kind = mpcSubscriptionIdentifier),
    initMQTTProperty(kind = mpcSharedSubAvailable),
    initMQTTProperty(kind = mpcServerKeepAlive),
    initMQTTProperty(kind = mpcRespInfo),
    initMQTTProperty(kind = mpcServerReference),
    initMQTTProperty(kind = mpcAuthMethod),
    initMQTTProperty(kind = mpcAuthData),
  ]

  result.variableHeader = MQTTConnackVarHeader(
    connackFlags: cFlags,
    connReasonCode: crSuccess,
    properties: MQTTProperties(length: encodeVarBytes(properties.sizeof), data: properties)
  )

proc initMQTTPublishPacket*(topicName: seq[Rune]): MQTTPublishPacket =
  result = MQTTPublishPacket()
  
  #Control type has a type opcode of 0x1 
  result.fixedHeader = MQTTPublishFixedHeader(
    header: MQTTPublishFixedHeaderFields(headerType: ctPublish, dup: 0x00, qosLevel: 00, retain: 0x00),
    remaining: @[]
  )

  let properties: seq[MQTTProperty] = @[
    initMQTTProperty(kind = mpcPayloadFormatIndicator),
    initMQTTProperty(kind = mpcMessageExpiryInterval),
    initMQTTProperty(kind = mpcTopicAlias),
    initMQTTProperty(kind = mpcResponseTopic),
    initMQTTProperty(kind = mpcPayloadFormatIndicator),
    initMQTTProperty(kind = mpcCorrelationData),
    initMQTTProperty(kind = mpcUserProperty),
    initMQTTProperty(kind = mpcSubscriptionIdentifier),
    initMQTTProperty(kind = mpcContentType),
  ]

  result.variableHeader = MQTTPublishVarHeader(
    topicName: topicName,
    packetId: 0,
    properties: MQTTProperties(length: encodeVarBytes(properties.sizeof), data: properties)
  )

  result.payload = BinData(length: 0, data: @[])

proc initMQTTPubackPacket*(): MQTTPubackPacket =
  result = MQTTPubackPacket()
  
  #Control type has a type opcode of 0x1 
  result.fixedHeader = MQTTFixedHeader(
    header: MQTTFixedHeaderFields(headerType: ctPuback, flags: 0),
    remaining: @[]
  )

  let properties: seq[MQTTProperty] = @[
    initMQTTProperty(kind = mpcReason),
    initMQTTProperty(kind = mpcUserProperty),
  ]

  result.variableHeader = MQTTPubackVarHeader(
    packetId: 0,
    pubAckReasonCode: paSuccess, #success default
    properties: MQTTProperties(length: encodeVarBytes(properties.sizeof), data: properties)
  )

proc initMQTTPubrecPacket*(): MQTTPubrecPacket =
  result = MQTTPubrecPacket()
  
  #Control type has a type opcode of 0x1 
  result.fixedHeader = MQTTFixedHeader(
    header: MQTTFixedHeaderFields(headerType: ctPubrec, flags: 0),
    remaining: @[]
  )

  let properties: seq[MQTTProperty] = @[
    initMQTTProperty(kind = mpcReason),
    initMQTTProperty(kind = mpcUserProperty)
  ]

  result.variableHeader = MQTTPubrecVarHeader(
    packetId: 0,
    pubRecReasonCode: prSuccess, #success default
    properties: MQTTProperties(length: encodeVarBytes(properties.sizeof), data: properties)
  )

proc initMQTTPubrelPacket*(): MQTTPubrelPacket =
  result = MQTTPubrelPacket()
  
  #Control type has a type opcode of 0x1 
  result.fixedHeader = MQTTFixedHeader(
    header: MQTTFixedHeaderFields(headerType: ctPubrel, flags: 0),
    remaining: @[]
  )

  let properties: seq[MQTTProperty] = @[
    initMQTTProperty(kind = mpcReason),
    initMQTTProperty(kind = mpcUserProperty)
  ]

  result.variableHeader = MQTTPubrelVarHeader(
    packetId: 0,
    pubRelReasonCode: prlSuccess, #success default
    properties: MQTTProperties(length: encodeVarBytes(properties.sizeof), data: properties)
  )

proc initMQTTPubcompPacket*(): MQTTPubcompPacket =
  result = MQTTPubcompPacket()
  
  #Control type has a type opcode of 0x1 
  result.fixedHeader = MQTTFixedHeader(
    header: MQTTFixedHeaderFields(headerType: ctPubcomp, flags: 0),
    remaining: @[]
  )

  let properties: seq[MQTTProperty] = @[
    initMQTTProperty(kind = mpcReason),
    initMQTTProperty(kind = mpcUserProperty)
  ]

  result.variableHeader = MQTTPubcompVarHeader(
    packetId: 0,
    pubCompReasonCode: pcSuccess, #success default
    properties: MQTTProperties(length: encodeVarBytes(properties.sizeof), data: properties)
  )


proc initMQTTSubscribePacket*(topics: seq[TopicFilterData]): MQTTSubscribePacket =
  result = MQTTSubscribePacket()
  
  #Control type has a type opcode of 0x1 
  result.fixedHeader = MQTTFixedHeader(
    header: MQTTFixedHeaderFields(headerType: ctSubscribe, flags: 0b0010),
    remaining: @[]
  )

  let properties: seq[MQTTProperty] = @[
    initMQTTProperty(kind = mpcSubscriptionIdentifier),
    initMQTTProperty(kind = mpcUserProperty)
  ]

  result.variableHeader = MQTTSubscribeVarHeader(
    packetId: 0,
    properties: MQTTProperties(length: encodeVarBytes(properties.sizeof), data: properties)
  )

  let topics = initSubscriptionTopicFilters(topics)

  result.payload = MQTTSubscribePayload(
    topics: topics
  )

proc initMQTTSubackPacket*(reasons: seq[MQTTSubackReasonCode]): MQTTSubackPacket =
  result = MQTTSubackPacket()
  
  #Control type has a type opcode of 0x1 
  result.fixedHeader = MQTTFixedHeader(
    header: MQTTFixedHeaderFields(headerType: ctSuback, flags: 0),
    remaining: @[]
  )

  let properties: seq[MQTTProperty] = @[
    initMQTTProperty(kind = mpcReason),
    initMQTTProperty(kind = mpcUserProperty)
  ]

  result.variableHeader = MQTTSubackVarHeader(
    packetId: 0,
    properties: MQTTProperties(length: encodeVarBytes(properties.sizeof), data: properties)
  )

  result.payload = MQTTSubackPayload(
    reasons: reasons
  )

proc initMQTTUnsubscribePacket*(unsubscribeTopicFilters: seq[UTF8String]): MQTTUnsubscribePacket =
  result = MQTTUnsubscribePacket()
  
  #Flags must be 0010
  result.fixedHeader = MQTTFixedHeader(
    header: MQTTFixedHeaderFields(headerType: ctUnsubscribe, flags: 0b0010),
    remaining: @[]
  )

  let properties: seq[MQTTProperty] = @[
    initMQTTProperty(kind = mpcUserProperty)
  ]

  result.variableHeader = MQTTUnsubscribeVarHeader(
    packetId: 0,
    properties: MQTTProperties(length: encodeVarBytes(properties.sizeof), data: properties)
  )

  result.payload = MQTTUnsubscribePayload(
    unsubscribeTopicFilters: initTopicFilters(unsubscribeTopicFilters)
  )

proc initMQTTUnsubackPacket*(reasons: seq[MQTTUnsubackReasonCode]): MQTTUnsubackPacket =
  result = MQTTUnsubackPacket()
  
  #Control type has a type opcode of 0x1 
  result.fixedHeader = MQTTFixedHeader(
    header: MQTTFixedHeaderFields(headerType: ctUnsuback, flags: 0),
    remaining: @[]
  )

  let properties: seq[MQTTProperty] = @[
    initMQTTProperty(kind = mpcReason),
    initMQTTProperty(kind = mpcUserProperty)
  ]

  result.variableHeader = MQTTUnsubackVarHeader(
    packetId: 0,
    properties: MQTTProperties(length: encodeVarBytes(properties.sizeof), data: properties)
  )

  result.payload = MQTTUnsubackPayload(
    reasons: reasons
  )

proc initMQTTPingReqPacket*(): MQTTPingReqPacket =
  result = MQTTPingReqPacket()

  result.fixedHeader = MQTTFixedHeader(
    header: MQTTFixedHeaderFields(headerType: ctPingreq, flags: 0),
    remaining: @[]
  )

proc initMQTTPingRespPacket*(): MQTTPingRespPacket =
  result = MQTTPingRespPacket()

  result.fixedHeader = MQTTFixedHeader(
    header: MQTTFixedHeaderFields(headerType: ctPingresp, flags: 0),
    remaining: @[]
  )

proc initMQTTDisconnectPacket*(reason: MQTTDisconnectReasonCode): MQTTDisconnectPacket =
  result = MQTTDisconnectPacket()

  result.fixedHeader = MQTTFixedHeader(
    header: MQTTFixedHeaderFields(headerType: ctDisconnect, flags: 0),
    remaining: @[]
  )

  let properties: seq[MQTTProperty] = @[
    initMQTTProperty(kind = mpcSessionExpiry),
    initMQTTProperty(kind = mpcReason),
    initMQTTProperty(kind = mpcUserProperty),
    initMQTTProperty(kind = mpcServerReference)
  ]

  result.variableHeader = MQTTDisconnectVarHeader(
    reason: reason,
    properties: MQTTProperties(length: encodeVarBytes(properties.sizeof), data: properties)
  )

proc initMQTTAuthPacket*(reason: MQTTAuthReasonCode): MQTTAuthPacket =
  result = MQTTAuthPacket()

  result.fixedHeader = MQTTFixedHeader(
    header: MQTTFixedHeaderFields(headerType: ctAuth, flags: 0),
    remaining: @[]
  )

  let properties: seq[MQTTProperty] = @[
    initMQTTProperty(kind = mpcAuthMethod),
    initMQTTProperty(kind = mpcAuthData),
    initMQTTProperty(kind = mpcReason),
    initMQTTProperty(kind = mpcUserProperty)
  ]

  result.variableHeader = MQTTAuthVarHeader(
    authReasonCode: reason,
    properties: MQTTProperties(length: encodeVarBytes(properties.sizeof), data: properties)
  )

when isMainModule:
  proc read() {.async.} =
    while true:
      discard sleepAsync(1000)
      let packet = initMQTTConnectPacket()
      #let (opcode, data) = await ws.readData()
      waitFor ws.sendText($packet)

  let a: cushort = htons(0x0001)
  var c: ptr char = cast[ptr char](unsafeaddr a)

  c = cast[ptr char](cast[uint64](c) + sizeof(char).uint64)

  var bs: MQTTVarByteStream = @[132.toMQTTVarByte, 133.toMQTTVarByte, 0x56.toMQTTVarByte]
  let val = decodeVarBytes(bs)

  var by = 128.toMQTTVarByte
  assert by.cont == 1
  assert by.bits == 0
  assert (by or 128) == 128

  var bya = 129.toMQTTVarByte
  assert bya.bits == 1
  assert bya.cont == 1

  let byc = 255.toMQTTVarByte
  assert byc.cont == 1
  assert byc.bits == 127

  assert (byc and 255) == 255
  assert (byc and 1) == 1
  assert (byc and 128) == 128

  let encoded = encodeVarBytes(val)
  assert encoded == bs

  echo sizeof(MQTTConnectFlags)
  let packet = initMQTTConnectPacket()
  echo $packet
  
  asyncCheck read()
  asyncCheck ping()
  runForever()
