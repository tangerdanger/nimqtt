#Thanks to https://xmonader.github.io/nimdays/day15_tcprouter.html for the pain free tutorial

import net, asyncdispatch, asyncnet, options, strformat, asynchttpserver, websocket, uri
from httpcore import parseheader

const defineSsl = defined(ssl) or defined(nimdoc)

echo "SSL? ", defineSsl

type
  ForwardOptions = object
    ## Options for forwarding
    listenAddr*: string
    listenPort*: Port
    toAddr*: string
    toPort*: Port

  Forwarder = object of RootObj
    ## Forwards websocket packets as defined in options
    options*: ForwardOptions

proc isClosed*(ws: AsyncWebSocket): bool = 
  ## Helper to see if a websocket is closed
  ws.sock.isClosed

proc forwardRequest*(this: ref Forwarder, server: AsyncSocket, request: FutureVar[Request], client: AsyncSocket) {.async.} =
  ## Verifies a valid websocket connection with our mqtt protocol and connects the client to the remote toAddr
  ## Checks if the client or remote has data and forwards all packets
  echo "Forwarding Request"

  while not client.isClosed:
    echo "Looping"
    #If our client isn't closed, we want to get the initial sent headers and add them to a request to deliver to the websocket server
    let message: string = await client.recvLine()
    echo message

    if message == "" or message == "\c\L":
      #End of headers
      break
    else:
      let (key, val) = parseHeader(message)
      request.mget().headers[key] = val
      echo key, val

  echo "Conncting with: ", $request.mget().headers
  #Opens a connection to the 
  let (clientWS, err) = await client.verifyWebsocketRequest(headers = request.mget().headers, protocol = "mqtt")

  if clientWS.isNil:
    #If you websocket client failed to verify the send headers, we close it
    echo "WS Negotiation Failure: ", err
    await request.mget().respond(Http400, "Websocket Negotiation Failure")
    request.mget().client.close()
    return

  client.close()
  echo "Client Connection Successful"
  let isSsl = client.isSsl
  let remoteWS = await newAsyncWebsocketClient(this.options.toAddr, this.options.toPort, path = "/", protocols = @["mqtt"], ssl = isSsl)
  echo "Remote Connection Successful"

  proc clientHasData() {.async.} =
    while not clientWS.isClosed and not remoteWS.isClosed:
      let data = await clientWS.readData()
      echo "Forward to Remote: (opcode: ", data.opcode, ", data: ", data.data.len, ")"
      if data.opcode == Opcode.Text:
        waitFor remoteWS.sendText(data.data)
      else:
        waitFor remoteWS.sendBinary(data.data)

  proc remoteHasData() {.async.} =
    while not remoteWS.isClosed and not clientWS.isClosed:
      let data = await remoteWS.readData()
      echo "Forward to Client: (opcode: ", data.opcode, ", data: ", data.data.len, ")"
      if data.opcode == Opcode.Text:
        waitFor clientWS.sendText(data.data)
      else:
        waitFor clientWS.sendBinary(data.data)


  #While this thread runs, we add more async checks to our global dispatcher
  try:
    asyncCheck clientHasData()
    asyncCheck remoteHasData()
  except:
    echo getCurrentExceptionMsg()

proc newForwarder*(opts: ForwardOptions): ref Forwarder =
  ## Given opts ForwardOptions, set up a forwarder to prepare to start listening
  result = new Forwarder
  result.options = opts

proc forward*(forwarder: ref Forwarder) {.async.} =
  ## Listen to our server's socket. This will forward any attempted connections to our forwarder
  ## A new connection indicates a new incoming websocket handshake

  #Our server socket listens for any connections from our designated listen address
  var server = newAsyncSocket(buffered = false)
  let sslCtx = newContext(protTLSv1, verifyMode = CVerifyNone)
  if defined(ssl):
    echo "Wrapping"
    sslCtx.wrapSocket(server)
  server.setSockOpt(OptReuseAddr, true) #We say yes to re-using sockets
  server.setSockOpt(OptReusePort, true) #We say yes to re-using Ports
  server.bindAddr(forwarder.options.listenPort, forwarder.options.listenAddr)
  server.listen()
  echo "Is server ssl? ", server.isSsl()

  while true:
    var (address, client) = await server.acceptAddr()
    echo "Connection from ", address
    sslCtx.wrapConnectedSocket(client, handshakeAsClient)
    echo "Is ssl? ", client.isSsl()
    #We want a request to build up our websocket handshake
    var request: FutureVar[Request] = newFutureVar[Request]("nimqtt.forwarder.forward")
    request.mget().url = initUri()
    request.mget().headers = newHttpHeaders()

    #Start forwarding requests from this client to target address
    discard forwarder.forwardRequest(server, request, client)
      
  #Finish up
  server.close()

when isMainModule:
  #Create a test websocket connection to act as a fake server in which to receive our forwarded data
  proc connect(req: Request) {.async.} =
    #Essentially the same as the websocket server example from niv's websocket library
    let (ws, error) = await verifyWebsocketRequest(req, "mqtt")
    if ws.isNil:
      echo "WS Negotiation failed: ", error
      await req.respond(Http400, "Websocket Negotiation Failed: " & error)
      req.client.close()

    else:
      while true:
        try:
          var data = await ws.readData()
          echo "ToAddr received data: (opcode: ", data.opcode, ", data: ", data.data.len, ")"

          if data.opcode == Opcode.Text:
            echo "Sending text back"
            waitFor ws.sendText("Thanks for the data: " & data.data)
          else:
            echo "Sending bin back"
            waitFor ws.sendBinary(data.data)
        except:
          echo getCurrentExceptionMsg()
          break
      discard ws.close()

  let forwardOptions = ForwardOptions(listenAddr: "127.0.0.1", listenPort: 11000.Port, toAddr: "127.0.0.1", toPort: 6969.Port)
  let forwarder = newForwarder(forwardOptions)
  var server = newAsyncHttpServer()

  #Asynccheck instead of discard. 
  #This asks our async dispatcher to continuously poll these async procs and run them in the background without blocking
  asyncCheck forwarder.forward()
  asyncCheck server.serve(forwarder.options.toPort, connect)
  runForever() #Blocks, running our AsyncCheck procs until stopped
