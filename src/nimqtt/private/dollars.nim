import strformat, strutils, unicode
import types
import helpers

proc `$`*(b: MQTTVarByte): string =
  &"Bits: {b.bits.toBin(7)}, Cont: {b.cont.toBin(1)}"

proc `$`*(fixedHeaderFields: MQTTFixedHeaderFields): string =
  &"\n\tHeader Type: {ord(fixedHeaderFields.headerType).toBin(4)}\n\tFlags: {fixedHeaderFields.flags.toBin(4)}"

proc `$`*(fHeader: MQTTFixedHeader): string =
  &"\t{fHeader.header}\n\tremaining: {fHeader.remaining}"

proc `$`*(flags: MQTTConnectFlags): string = 
  let val = (flags.reserved shl 7) or (flags.cleanStart shl 6) or (flags.willFlag shl 5) or (flags.willQos shl 4) or (flags.willRetain shl 2) or (flags.password shl 1) or (flags.username)
  &"{val.toBin(8)}"

proc `$`*(p: UTF8StringPair): string = 
  &"{$p[0]}{$p[1]}"

proc toUgly(prop: MQTTProperty): string =
  result = ""
  case prop.kind:
  of mpcPayloadFormatIndicator, mpcReqInfo, mpcReqRespInfo, mpcMaxQOS, mpcRetainAvailable, mpcWildcardSubAvailable, mpcSubIdAvailable, mpcSharedSubAvailable:
    result = $prop.data8
  of mpcServerKeepAlive, mpcReceiveMax, mpcTopicAliasMax, mpcTopicAlias:
    result = $prop.data16
  of mpcMessageExpiryInterval, mpcSessionExpiry, mpcWillDelayInterval, mpcMaxPackSize:
    result = $prop.data32
  of mpcContentType, mpcResponseTopic, mpcAssignedClientId, mpcRespInfo, mpcServerReference, mpcReason, mpcAuthMethod:
    result = $prop.dataUTF
  of mpcUserProperty:
    result = $prop.dataUTFP
  of mpcSubscriptionIdentifier:
    result = $prop.dataVar
  of mpcCorrelationData, mpcAuthData:
    result = $prop.dataBin

proc `$`*(prop: MQTTProperty): string =
  &"\n\t\t\t{{ID: {prop.id}, kind: {prop.kind}, data: {prop.toUgly}}}"

proc `$`*(s: seq[MQTTProperty]): string =
  result = ""
  for i in s:
    result.add(&"\t\t\t{i}")

proc `$`*(props: MQTTProperties): string =
  &"\n\t\tLength: {props.length}, \n\t\tProperties: [{props.data}\n\t\t]"

proc `$`*(conHeader: MQTTConnectVarHeader): string =
  &"Protocol Name: {conHeader.protocolName}\n\tConnect Flags: {conHeader.connectFlags}\n\tKeepAlive: {conHeader.keepAlive}\n\tProperties: [{conHeader.properties}\n]"

proc `$`*(p: MQTTConnectPayload): string =
  &"[\n\tClient ID: {$p.clientId}\n\tWill: {p.will}\n\tWill Topic: {p.willTopic}\n\tWill Payload: {p.willPayload}\n\tUsername: {p.username}\n\tPassword: {p.password}\n]"

proc `$`*(packet: MQTTConnectPacket): string =
  &"===========\nFixed Header: {packet.fixedHeader}\nVar Header: \n\t{packet.variableHeader}\nPayload: {packet.payload}\n========"

