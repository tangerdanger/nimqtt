import strutils, unicode
import types

proc toBin*(v: uint, places: int): string =
  v.int.toBin(places)

proc `and`*(a: MQTTVarByte, b: uint): uint =
  (a.bits or (a.cont shl 7)).uint and b

proc `or`*(a: MQTTVarByte, b: uint): uint =
  (a.bits or (a.cont shl 7)).uint or b

template doWhile*(a, b: untyped): untyped =
  b
  while a:
    b

proc `/`*(x: byte, y: uint8): float =
  x.uint8 / y

proc toMQTTVarByte*(x: uint): MQTTVarByte =
  result.bits = x.uint8 and 127 #Extract continue bit
  result.cont = (x.uint8 shr 7)

proc toUTF8String*(runes: seq[Rune]): UTF8String =
  ## Packs a seq or unicode runes into a UTF8String for binary transfer
  result.data = runes
  result.msb = (runes.len and 0xff).uint8
  result.lsb = (runes.len shr 8).uint8

let 
  procotolName*: MQTTProtocolName = "MQTT".toRunes.toUTF8String
  protocolVersion*: MQTTProtocolVersion = 0x0005 #Version 5 (00000101)
