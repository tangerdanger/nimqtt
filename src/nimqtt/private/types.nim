## Packet definitions for MQTT Control types and associated fields

import unicode
const KeepAlive* = 60 * 3 #3 min Keepalive

type
  #Implementation specific types
  DecodeError* = object of Exception
  MQTTProtocolName* = UTF8String
  MQTTProtocolVersion* = uint8

  #Data Types
  MQTTTopicFilter* {.bycopy, packed.} = object
    topic*: UTF8String
  
  UTF8String* {.packed.} = object
    msb*: uint8
    lsb*: uint8
    data*: seq[Rune]

  UTF8StringPair* = array[2, UTF8String] ## A pair of UTF-8 Strings

  BinData* {.bycopy, packed.} = object
    ## Binary Data of variable length
    length*: cushort
    data*: seq[uint8]

  MQTTVarByte* {.packed.} = object
    ## A var byte is a packed int that fits into a single byte
    bits* {.bitsize:7.}: cuint
    cont* {.bitsize: 1.}: cuint

  MQTTVarByteStream* = seq[MQTTVarByte] ## A stream of VarBytes

  TopicFilterData* = tuple[topic: UTF8String, reserved, retainHandling, rap, nl, qos: uint8]

  MQTTPropertyCode* {.size: sizeof(uint8).} = enum
    ## Kinds of packet property codes
    mpcPayloadFormatIndicator = (0x01, "Payload Format Indicator"),
    mpcMessageExpiryInterval = (0x02, "Message Expiry Interval") ,
    mpcContentType = (0x03, "ContentType"),
    mpcResponseTopic = (0x08, "Response Topic"),
    mpcCorrelationData = (0x09, "Correlation Data"),
    mpcSubscriptionIdentifier = (0x0B, "Subscription Identifier"),
    mpcSessionExpiry = (0x11, "Session Expiry"),
    mpcAssignedClientId = (0x12, "Assigned Client ID"),
    mpcServerKeepAlive = (0x13, "Server Keep-Alive"),
    mpcAuthMethod = (0x15, "Auth Method"),
    mpcAuthData = (0x16, "Auth Data"),
    mpcReqInfo = (0x17, "Request Info"),
    mpcWillDelayInterval = (0x18, "Will Delay Interval"),
    mpcReqRespInfo = (0x19, "Request Resp Info"),
    mpcRespInfo = (0x1A, "Response Info"),
    mpcServerReference = (0x1C, "Server Reference"),
    mpcReason = (0x1F, "Reason"),
    mpcReceiveMax = (0x21, "Receive Max"),
    mpcTopicAliasMax = (0x22, "Topic Alias Max"),
    mpcTopicAlias = (0x23, "Topic Alias"),
    mpcMaxQOS = (0x24, "Max QoS"),
    mpcRetainAvailable = (0x25, "Retain Available"),
    mpcUserProperty = (0x26, "User Property"),
    mpcMaxPackSize = (0x27, "Max Pack Size"),
    mpcWildcardSubAvailable = (0x28, "Wildcard Sub Available"),
    mpcSubIdAvailable = (0x29, "Sub Id Available"),
    mpcSharedSubAvailable = (0x2A, "Shared Sub Available")

  MQTTControlType* {.size: sizeof(uint8).} = enum
    ## Code for each packet type
    ctReserved = (0, "Reserved"),
    ctConnect = "Connect",
    ctConnack = "Connack",
    ctPublish = "Publish",
    ctPuback = "Puback", 
    ctPubrec = "Pubrec",
    ctPubrel = "Pubrel",
    ctPubcomp = "Pubcomp",
    ctSubscribe = "Subscribe",
    ctSuback = "Suback",
    ctUnsubscribe = "Unsubscribe",
    ctUnsuback = "Unsuback",
    ctPingreq = "Pingreq",
    ctPingresp = "Pingresp",
    ctDisconnect = "Disconnect",
    ctAuth = (15, "Auth")

  MQTTConnackReasonCode* {.size: sizeof(uint8).} = enum
    ## Connack Reason codes
    crSuccess = (0x00, "Success"),
    crUnspecifiedError = (0x80, "Unspecified Error"),
    crMalformedPacket = (0x81, "Malformed Packet"),
    crProtocolError = (0x82, "Protocol Error"),
    crImplSpecificError = (0x83, "Impl Specific Error"),
    crUnsupportedProtocolVersion = (0x84, "Unsupported Protocol Version"),
    crClientIDInvalid = (0x85, "Client ID Invalid"),
    crBadAuth = (0x86, "Bad Auth"),
    crNoAuth = (0x87, "Not Authorized"),
    crServerUnavailable = (0x88, "Server Unavailable"),
    crServerBusy = (0x89, "Server Busy"),
    crBanned = (0x8a, "Banned"),
    crBadAuthMethod = (0x8c, "Bad Auth Method"),
    crTopicNameInvalid = (0x90, "Topic Name Invalid"),
    crPacketTooBig = (0x95, "Packet Too Big"),
    crQuotaExceeded = (0x97, "Quota Exceeded"),
    crInvalidPayloadFormat = (0x99, "Invalid Payload Format"),
    crRetainNotSupported = (0x9a, "Retain Not Supported"),
    crQosNotSupported = (0x9b, "QoS Not Supported"),
    crUseAnotherServer = (0x9c, "Use Another Server"),
    crServerMoved = (0x9d, "Server Moved"),
    crConnRateExceeded = (0x9f, "Conn Rate Exceeded")

  MQTTPubackReasonCode* {.size: sizeof(uint8).} = enum
    ## Puback reason codes
    paSuccess = (0x00, "Success"),
    paNoMatchingSubs = (0x10, "No Matching Subscribers"),
    paUnspecifiedError = (0x80, "Unspecified Error"),
    paImplementationError = (0x83, "Implementation Specific Error"),
    paNotAuthorized = (0x87, "Not Authorized"),
    paTopicNameInvalid = (0x90, "Topic Name Invalid"),
    paPacketIdInUse = (0x91, "Packet Id In Use"),
    paQuotaExceeded = (0x97, "Quota Exceeded"),
    paPayloadFormatInvalid = (0x99, "Invalid Payload Format")

  MQTTPubrecReasonCode* {.size: sizeof(uint8).} = enum
    ## Pubrec reason codes
    prSuccess = (0x00, "Success"),
    prNoMatchingSubs = (0x10, "No Matching Subscribers"),
    prUnspecifiedError = (0x80, "Unspecified Error"),
    prImplementationError = (0x83, "Implementation Specific Error"),
    prNotAuthorized = (0x87, "Not Authorized"),
    prTopicNameInvalid = (0x90, "Topic Name Invalid"),
    prPacketIdInUse = (0x91, "Packet Id In Use"),
    prQuotaExceeded = (0x97, "Quota Exceeded"),
    prPayloadFormatInvalid = (0x99, "Invalid Payload Format")

  MQTTPubrelReasonCode* {.size: sizeof(uint8).} = enum
    ## Pubrel reason codes
    prlSuccess = (0x00, "Success"),
    prlPacketIdNotFound = (0x92, "Packet Id Not Found")

  MQTTPubcompReasonCode* {.size: sizeof(uint8).} = enum
    ## Pubcomp reason codes
    pcSuccess = (0x00, "Success"),
    pcPacketIdNotFound = (0x92, "Packet Id Not Found")

  MQTTSubackReasonCode* {.size: sizeof(uint8).} = enum
    ## Pubcomp reason codes
    saGrantedQos0 = (0x00, "Granted QoS 0"),
    saGrantedQos1 = (0x01, "Granted QoS 1"),
    saGrantedQos2 = (0x02, "Granted QoS 2"),
    saUnspecifiedError = (0x80, "Unspecified Error"),
    saImplementationSpecificError = (0x83, "Implementation Specific Error"),
    saNotAuthorized = (0x87, "Not Authorized"),
    saTopicFilterInvalid = (0x8e, "Topic Filter Invalid"),
    saPacketIdInUse = (0x91, "Packet Id In Use"),
    saQuotaExceeded = (0x97, "Quota Exceeded"),
    saSharedSubNotSupported = (0x9e, "Shared Sub Not Supported"),
    saSubIdNotSupported = (0xa1, "Subscription ID Not Supported"),
    saWildcardSubscriptionNotSupported = (0xa2, "Wildcard Subscription Not Supported")

  MQTTUnsubackReasonCode* {.size: sizeof(uint8).} = enum
    ## Pubcomp reason codes
    usaSuccess = (0x00, "Success"),
    usaNoSubscriptionExists = (0x11, "No Subscription Exists"),
    usaUnspecifiedError = (0x80, "Unspecified Error"),
    usaImplementationSpecificError = (0x83, "Implementation Specific Error"),
    usaNotAuthorized = (0x87, "Not Authorized"),
    usaTopicFilterInvalid = (0x8f, "Topic Filter Invalid"),
    usaPacketIdInUse = (0x91, "Packet Id In Use")

  MQTTDisconnectReasonCode* {.size: sizeof(uint8).} = enum
    dcNormalDisconnect = (0x00, "Normal Disconnect"),
    dcDisconnectWithWillMessage = (0x04, "The client wishes to disconnect"),
    dcUnspecifiedError = (0x80, "Unspecified Error"),
    dcMalformedPacket = (0x81, "Malformed Packet"),
    dcProtocolError = (0x82, "Protocol Error"),
    dcImplementationSpecificError = (0x83, "Implementation Specific Error"),
    dcNotAuthorized = (0x87, "Not Authorized"),
    dcServerBusy = (0x89, "Server Busy"),
    dcServerShuttingDown = (0x8b, "Server Shutdown"),
    dcKeepAliveTimeout = (0x8d, "KeepAlive Timeout"),
    dcSessionTakenOver = (0x8e, "Session Taken Over"),
    dcTopicFilterInvalid = (0x8f, "Topic Filter Invalid"),
    dcTopicNameInvalid = (0x90, "Topic Name Invalid"),
    dcReceiveMaximumExceeded = (0x93, "Receive Maximum Exceeded"),
    dcTopicAliasInvalid = (0x94, "Topic Alias Invalid"),
    dcPacketTooLarge = (0x95, "Packet Too Large"),
    dcMessageRateTooHigh = (0x96, "Message Rate Too High"),
    dcQuoteExceeded = (0x97, "Quote Exceeded"),
    dcAdminAction = (0x98, "Admin Action"),
    dcPayloadFormatInvalid = (0x99, "Payload Format Invalid"),
    dcRetainNotSupported = (0x9a, "Retain Not Supported"),
    dcQosNotSupported = (0x9b, "Qos Not Supported"),
    dcUseAnotherServer = (0x9c, "Use Another Server"),
    dcServerMoved = (0x9d, "Server Moved"),
    dcSharedSubNotSupported = (0x9e, "Shared Subscription Not Supported"),
    dcConnectionRateExceeded = (0x9f, "Connection Rate Exceeded"),
    dcMaximumConnectTime = (0xa0, "Maximum Connect Time"),
    dcSubIdNotSupported = (0xa1, "Sub Id Not Supported"),
    dcWildcardSubscriptionNotSupported = (0xa2, "Wildcard Subscription Not Supported")

  MQTTAuthReasonCode* {.size: sizeof(uint8).} = enum
    arSuccess = (0x00, "Success"),
    arContinueAuthentication = (0x81, "Continue Authentication"),
    arReauthenticate = (0x91, "Reauthenticate")

  MQTTFixedHeaderFields* {.packed.} = object
    ## Fixed header fields control the control type code
    ## and various flags
    headerType* {.bitsize:4.}: MQTTControlType
    flags* {.bitsize:4.}: cuint

  #Generic flags for Control types

  MQTTFixedHeader* {.bycopy, packed.} = object
    ## A fixed header with the control type, flags
    ## and remaining length
    header*: MQTTFixedHeaderFields
    remaining*: MQTTVarByteStream

  MQTTProperty* {.bycopy, packed.} = object
    ## An MQTT Property with it's designated data type
    id*: MQTTVarByteStream
    case kind*: MQTTPropertyCode
    of mpcPayloadFormatIndicator, mpcReqInfo, mpcReqRespInfo, mpcMaxQOS, mpcRetainAvailable, mpcWildcardSubAvailable, mpcSubIdAvailable, mpcSharedSubAvailable:
      data8*: uint8 #single byte
    of mpcServerKeepAlive, mpcReceiveMax, mpcTopicAliasMax, mpcTopicAlias:
      data16*: cushort #2 bytes
    of mpcMessageExpiryInterval, mpcSessionExpiry, mpcWillDelayInterval, mpcMaxPackSize:
      data32*: uint32 #4 bytes
    of mpcContentType, mpcResponseTopic, mpcAssignedClientId, mpcRespInfo, mpcServerReference, mpcReason, mpcAuthMethod:
      dataUTF*: seq[Rune] #UTF-8 string
    of mpcUserProperty:
      dataUTFP*: UTF8StringPair #UTF-8 string pair
    of mpcSubscriptionIdentifier:
      dataVar*: MQTTVarByteStream #Var byte
    of mpcCorrelationData, mpcAuthData:
      dataBin*: BinData

  MQTTProperties* {.bycopy, packed.} = object 
    ## Generic Properties for packets
    length*: MQTTVarByteStream
    data*: seq[MQTTProperty]

  #Connect structures
  
  MQTTConnectFlags* {.bycopy, packed.} = object
    reserved* {.bitsize:1.}: uint8
    cleanStart* {.bitsize:1.}: uint8
    willFlag* {.bitsize:1.}: uint8
    willQos* {.bitsize:2.}: uint8
    willRetain* {.bitsize:1.}: uint8
    password* {.bitsize:1.}: uint8
    username* {.bitsize:1.}: uint8

  MQTTKeepAlive* {.bycopy, packed.} = object
    kaMSB*, kaLSB*: uint8

  MQTTConnectPayload* {.bycopy, packed.} = object
    clientId*: seq[Rune]
    will*: MQTTProperties
    willTopic*: seq[Rune]
    willPayload*: BinData
    username*: seq[Rune]
    password*: BinData 

  MQTTConnectVarHeader* {.bycopy, packed.} = object
    protocolName*: MQTTProtocolName
    connectFlags*: MQTTConnectFlags
    keepAlive*: MQTTKeepAlive
    properties*: MQTTProperties

  MQTTConnectPacket* {.bycopy, packed.} = object
    fixedHeader*: MQTTFixedHeader
    variableHeader*: MQTTConnectVarHeader
    payload*: MQTTConnectPayload

  #Connack

  MQTTConnackFlags* {.bycopy, packed.} = object
    reserved* {.bitsize:7.}: uint8
    sessionPresent* {.bitsize:1.}: uint8

  MQTTConnackVarHeader* {.bycopy, packed.} = object
    connackFlags*: MQTTConnackFlags
    connReasonCode*: MQTTConnackReasonCode
    properties*: MQTTProperties

  MQTTConnackPacket* {.bycopy, packed.} = object
    fixedHeader*: MQTTFixedHeader
    variableHeader*: MQTTConnackVarHeader

  #Publish

  MQTTPublishFixedHeaderFields* {.packed.} = object
    headerType* {.bitsize:4.}: MQTTControlType
    dup* {.bitsize:1.}: cuint
    qosLevel* {.bitsize:2.}: cuint
    retain* {.bitsize:1.}: cuint

  MQTTPublishFixedHeader* {.bycopy, packed.} = object
    header*: MQTTPublishFixedHeaderFields
    remaining*: MQTTVarByteStream

  MQTTPublishVarHeader* {.bycopy, packed.} = object
    topicName*: seq[Rune]
    packetId*: cushort
    properties*: MQTTProperties

  MQTTPublishPacket* {.bycopy, packed.} = object
    fixedHeader*: MQTTPublishFixedHeader #Use custom fixed header
    variableHeader*: MQTTPublishVarHeader
    payload*: BinData

  #Puback

  MQTTPubackVarHeader* {.bycopy, packed.} = object
    packetId*: cushort
    pubAckReasonCode*: MQTTPubackReasonCode
    properties*: MQTTProperties

  MQTTPubackPacket* {.bycopy, packed.} = object
    fixedHeader*: MQTTFixedHeader 
    variableHeader*: MQTTPubackVarHeader

  #PubRec 

  MQTTPubrecVarHeader* {.bycopy, packed.} = object
    packetId*: cushort
    pubRecReasonCode*: MQTTPubrecReasonCode
    properties*: MQTTProperties

  MQTTPubrecPacket* {.bycopy, packed.} = object
    fixedHeader*: MQTTFixedHeader
    variableHeader*: MQTTPubrecVarHeader

  #Pubrel
  
  MQTTPubrelVarHeader* {.bycopy, packed.} = object
    packetId*: cushort
    pubRelReasonCode*: MQTTPubrelReasonCode
    properties*: MQTTProperties

  MQTTPubRelPacket* {.bycopy, packed.} = object
    fixedHeader*: MQTTFixedHeader
    variableHeader*: MQTTPubrelVarHeader

  #Pubcomp

  MQTTPubcompVarHeader* {.bycopy, packed.} = object
    packetId*: cushort
    pubCompReasonCode*: MQTTPubcompReasonCode
    properties*: MQTTProperties

  MQTTPubcompPacket* {.bycopy, packed.} = object
    fixedHeader*: MQTTFixedHeader
    variableHeader*: MQTTPubcompVarHeader

  #Subscribe

  MQTTSubscribeOptions* {.bycopy, packed.} = object
    reserved* {.bitsize:2.}: uint8
    retainHandling* {.bitsize:2.}: uint8
    rap* {.bitsize:1.}: uint8
    nl* {.bitsize:1.}: uint8
    qos* {.bitsize:2.}: uint8

  MQTTSubscriptionTopicFilter* {.bycopy, packed.} = object
    topicFilter*: UTF8String
    subscriptionOptions*: MQTTSubscribeOptions

  MQTTSubscribePayload* {.bycopy, packed.} = object
    topics*: seq[MQTTSubscriptionTopicFilter]

  MQTTSubscribeVarHeader* {.bycopy, packed.} = object
    packetId*: cushort  
    properties*: MQTTProperties

  MQTTSubscribePacket* {.bycopy, packed.} = object
    fixedHeader*: MQTTFixedHeader
    variableHeader*: MQTTSubscribeVarHeader
    payload*: MQTTSubscribePayload

  #Suback

  MQTTSubackPayload* {.bycopy, packed.} = object
    reasons*: seq[MQTTSubackReasonCode]

  MQTTSubackVarHeader* {.bycopy, packed.} = object
    packetId*: cushort  
    properties*: MQTTProperties

  MQTTSubackPacket* {.bycopy, packed.} = object
    fixedHeader*: MQTTFixedHeader
    variableHeader*: MQTTSubackVarHeader
    payload*: MQTTSubackPayload

  #Unsubscribe

  MQTTUnsubscribePayload* {.bycopy, packed.} = object
    unsubscribeTopicFilters*: seq[MQTTTopicFilter]

  MQTTUnsubscribeVarHeader* {.bycopy, packed.} = object
    packetId*: cushort  
    properties*: MQTTProperties

  MQTTUnsubscribePacket* {.bycopy, packed.} = object
    fixedHeader*: MQTTFixedHeader
    variableHeader*: MQTTUnsubscribeVarHeader
    payload*: MQTTUnsubscribePayload

  #Unsuback

  MQTTUnsubackPayload* {.bycopy, packed.} = object
    reasons*: seq[MQTTUnsubackReasonCode]

  MQTTUnsubackVarHeader* {.bycopy, packed.} = object
    packetId*: cushort  
    properties*: MQTTProperties

  MQTTUnsubackPacket* {.bycopy, packed.} = object
    fixedHeader*: MQTTFixedHeader
    variableHeader*: MQTTUnsubackVarHeader
    payload*: MQTTUnsubackPayload

  #PingREQ

  MQTTPingReqPacket* {.bycopy, packed.} = object
    fixedHeader*: MQTTFixedHeader

  #PingRESP

  MQTTPingRespPacket* {.bycopy, packed.} = object
    fixedHeader*: MQTTFixedHeader

  #Disconnect

  MQTTDisconnectVarHeader* {.bycopy, packed.} = object
    reason*: MQTTDisconnectReasonCode
    properties*: MQTTProperties

  MQTTDisconnectPacket* {.bycopy, packed.} = object
    fixedHeader*: MQTTFixedHeader
    variableHeader*: MQTTDisconnectVarHeader

  #Auth

  MQTTAuthVarHeader* {.bycopy, packed.} = object
    authReasonCode*: MQTTAuthReasonCode
    properties*: MQTTProperties

  MQTTAuthPacket* {.bycopy, packed.} = object
    fixedHeader*: MQTTFixedHeader
    variableHeader*: MQTTAuthVarHeader

