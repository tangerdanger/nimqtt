import websocket, asynchttpserver, asyncnet, asyncdispatch

var server = newAsyncHttpServer()

proc connect(req: Request) {.async.} =
  let (ws, error) = await verifyWebsocketRequest(req, "mqtt")
  if ws.isNil:
    echo "WS Negotiation failed: ", error
    await req.respond(Http400, "Websocket Negotiation Failed: " & error)
    req.client.close()

  else:
    while true:
      try:
        var data = await ws.readData()
        echo "(opcode: ", data.opcode, ", data: ", data.data.len, ")"

        if data.opcode == Opcode.Text:
          waitFor ws.sendText("Thanks for the data")
        else:
          waitFor ws.sendBinary(data.data)
      except:
        echo getCurrentExceptionMsg()
        break
    discard ws.close()

when isMainModule:
  waitFor server.serve(Port(8080), connect)

