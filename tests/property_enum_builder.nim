import macros, tables

type
  MQTTPropertyCode {.size: sizeof(cuchar).} = enum
    mpcPayloadFormatIndicator = 0x01,
    mpcMessageExpiryInterval = 0x02,
    mpcContentType = 0x03,
    mpcResponseTopic = 0x08,
    mpcCorrelationData = 0x09,
    mpcSubscriptionIdentifier = 0x0B,
    mpcSessionExpiry = 0x11,
    mpcAssignedClientId = 0x12,
    mpcServerKeepAlive = 0x13,
    mpcAuthMethod = 0x15,
    mpcReqInfo = 0x17,
    mpcWillDelayInterval = 0x18,
    mpcReqRespInfo = 0x19,
    mpcRespInfo = 0x1A,
    mpcServerReference = 0x1C,
    mpcReson = 0x1F,
    mpcReceiveMax = 0x21,
    mpcTopicAliasMax = 0x22,
    mpcTopicAlias = 0x23,
    mpcMaxQOS = 0x24,
    mpcRetainAvailable = 0x25,
    mpcUserProperty = 0x26,
    mpcMaxPackSize = 0x27,
    mpcWildcardSubAvailable = 0x28,
    mpcSubIdAvailable = 0x29,
    mpcSharedSubAvailable = 0x2A

dumpTree:
  let
    typeRefTable = {
      mpcPayloadFormatIndicator: cuchar
    }.toTable

