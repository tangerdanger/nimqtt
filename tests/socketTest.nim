import asyncnet, asyncdispatch, net, websocket, asynchttpserver, strutils, uri
from httpcore import parseHeader

const
  CONN_PORT = 6969.Port
  CONN_ADDR = "localhost"
  CONN_TO_ADDR = "127.0.0.1"
  CONN_TO_PORT = 7696.Port

proc forwardRequest(server: AsyncSocket, request: FutureVar[Request], client: AsyncSocket, address: string) {.async.} =
  var fullMessage: string = ""
  while not client.isClosed:
    let message: string = await client.recvLine()

    if message == "" or message == "\c\L":
      break
    else:
      fullMessage.add(message)
      let (key, val) = parseHeader(message)
      request.mget().headers[key] = val
      echo request.mget().headers
  let (ws, err) = await client.verifyWebsocketRequest(headers = request.mget().headers, protocol = "mqtt")
  if ws.isNil:
    echo "WS Negotiation Failure: ", err
    await request.mget().respond(Http400, "Websocket Negotiation Failure")
    request.mget().client.close()
  else:
    let remote = await newAsyncWebsocketClient(CONN_TO_ADDR, CONN_TO_PORT, path = "/", protocols = @["mqtt"])
    while true:
      try:
        let data = await ws.readData()
        echo "Forward: (opcode: ", data.opcode, ", data: ", data.data.len, ")"
        if data.opcode == Opcode.Text:
          waitFor remote.sendText(data.data)
        else:
          waitFor remote.sendBinary(data.data)
      except:
        echo getCurrentExceptionMsg()
        break

  await ws.close()

proc serve*() {.async.} =
  var server = newAsyncSocket(buffered = false)
  server.setSockOpt(OptReuseAddr, true)
  server.bindAddr(CONN_PORT, CONN_ADDR)

  server.listen()
  while true:
    let client = await server.accept()
    var request: FutureVar[Request] = newFutureVar[Request]("socketTest.serve")
    request.mget().url = initUri()
    request.mget().headers = newHttpHeaders()

    discard forwardRequest(server, request, client, CONN_ADDR)
      
  server.close()

proc connect(req: Request) {.async.} =
  let (ws, error) = await verifyWebsocketRequest(req, "mqtt")
  if ws.isNil:
    echo "WS Negotiation failed: ", error
    await req.respond(Http400, "Websocket Negotiation Failed: " & error)
    req.client.close()

  else:
    while true:
      try:
        var data = await ws.readData()
        echo "Server: (opcode: ", data.opcode, ", data: ", data.data.len, ")"

        if data.opcode == Opcode.Text:
          waitFor ws.sendText("Thanks for the data")
        else:
          waitFor ws.sendBinary(data.data)
      except:
        echo getCurrentExceptionMsg()
        break
    discard ws.close()


when isMainModule:
  var server = newAsyncHttpServer()
  asyncCheck serve()
  waitFor server.serve(CONN_TO_PORT, connect)
  runForever()
